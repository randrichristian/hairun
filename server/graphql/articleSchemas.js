var GraphQLSchema = require('graphql').GraphQLSchema;
var GraphQLObjectType = require('graphql').GraphQLObjectType;
var GraphQLList = require('graphql').GraphQLList;
var GraphQLObjectType = require('graphql').GraphQLObjectType;
var GraphQLNonNull = require('graphql').GraphQLNonNull;
var GraphQLID = require('graphql').GraphQLID;
var GraphQLString = require('graphql').GraphQLString;
var GraphQLDate = require('graphql-date');
var ArticleModel = require('../models/Article');

//Type des colonnes
var articleType = new GraphQLObjectType({
    name: 'article',
    fields: function () {
      return {
        _id: {
          type: GraphQLString
        },
        title: {
          type: GraphQLString
        },
        author: {
          type: GraphQLString
        },
        content: {
          type: GraphQLString
        },
        publisher: {
          type: GraphQLDate
        }
      }
    }
  });

//find by ID
  var queryType = new GraphQLObjectType({
    name: 'Query',
    fields: function () {
      return {
        articles: {
          type: new GraphQLList(articleType),
          resolve: function () {
            const articles = ArticleModel.find().exec()
            if (!articles) {
              throw new Error('Error')
            }
            return articles
          }
        },
        article: {
          type: articleType,
          args: {
            id: {
              name: '_id',
              type: GraphQLString
            }
          },
          resolve: function (root, params) {
            const articleDetails = ArticleModel.findById(params.id).exec()
            if (!articleDetails) {
              throw new Error('Error')
            }
            return articleDetails
          }
        }
      }
    }
  });

//nouveau - maj - delete
  var mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: function () {
      return {
        addArticle: {
          type: articleType,
          args: {
            title: {
              type: new GraphQLNonNull(GraphQLString)
            },
            author: {
              type: new GraphQLNonNull(GraphQLString)
            },
            content: {
              type: new GraphQLNonNull(GraphQLString)
            }
          },
          resolve: function (root, params) {
            const articleModel = new ArticleModel(params);
            const newArticle = articleModel.save();
            if (!newArticle) {
              throw new Error('Error');
            }
            return newArticle
          }
        },
        updateArticle: {
          type: articleType,
          args: {
            id: {
              name: 'id',
              type: new GraphQLNonNull(GraphQLString)
            },
            title: {
              type: new GraphQLNonNull(GraphQLString)
            },
            author: {
              type: new GraphQLNonNull(GraphQLString)
            },
            content: {
              type: new GraphQLNonNull(GraphQLString)
            }
          },
          resolve(root, params) {
            return ArticleModel.findByIdAndUpdate(params.id, { title: params.title, author: params.author, content: params.content, publisher: new Date() }, function (err) {
              if (err) return next(err);
            });
          }
        },
        removeArticle: {
          type: articleType,
          args: {
            id: {
              type: new GraphQLNonNull(GraphQLString)
            }
          },
          resolve(root, params) {
            const remArticle = ArticleModel.findByIdAndRemove(params.id).exec();
            if (!remArticle) {
              throw new Error('Error')
            }
            return remArticle;
          }
        }
      }
    }
  });

  module.exports = new GraphQLSchema({query: queryType, mutation: mutation});