var mongoose = require('mongoose');

// schéma de donées mongodb : article
var ArticleSchema = new mongoose.Schema({
  id: String,
  title: String,
  author: String,
  content: String,
  publisher: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Article', ArticleSchema);