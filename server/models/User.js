var mongoose = require('mongoose');

// schéma de donées mongodb : user
var UserSchema = new mongoose.Schema({
  id: String,
  name: String,
  lastname: String,
  login: String,
  pwd: String,
  creation_date: { type: Date, default: Date.now },
});

module.exports = mongoose.model('User', UserSchema);