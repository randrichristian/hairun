# TEST HAIRUN TECHNOLOGY

## Technologies 
- Langages : Typescript/Ecmascript
- Client : React, Client GRAPHQL
- Serveur : Framework nodeJS permettant l'utilisation de graphQL
- API : GRAPHQL
- Base de données : MongoDB

## Installation
Récupérez le code source

Dans chaque répertoire du projet, Client et Server, vous devez d'abord installer les dépendances nécessaires grâce à la commande : 
npm install

## Exécution
Vous pouvez exécuter l'application en entrant la commande

### Exécution serveur
npx nodemon

### Exécution client 
npm start

Ouvrez http://localhost:3000 pour l’afficher dans le navigateur
API article http://localhost:3000/graphql
API User http://localhost:3000/graphqlUser

Mais avant de tester la partie cliente, insérer des données à partir de ces API
#### Article
mutation {
  addArticle(
    title: "CRÉER UNE BASE DE DONNÉES DANS ACCESS",
    author: "Mr Jean",
    content: "Dans cet article, vous trouverez le premier tutoriel d’une série sur le logiciel Access. Dans ces derniers, vous découvrirez les fonctions principales d’Access. A savoir la création de base de données, de tables, les relations entre les tables, créer des formulaires, créer des requêtes, des états… une multitude de choses en somme. Ce premier tutoriel permettra de créer une base de données Access."
  ) {
    publisher
  }
}

### User 
mutation {
  addUser(
  	name : "RANDRIAMIALY",
    lastname : "Valery Christian",
    login : "admin",
    pwd : "admin"
    
  ) {
    creation_date
  }
}

## Auteurs
RANDRIAMIALY Valery Christian


