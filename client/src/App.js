import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './App.css';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';

const GET_ARTICLES = gql`
  {
    articles {
      _id
      title
      author
      publisher
    }
  }
`;

class App extends Component {

  render() {
    return (
      <Query pollInterval={500} query={GET_ARTICLES}>
        {({ loading, error, data }) => {
          if (loading) return 'Loading...';
          if (error) return `Error! ${error.message}`;
    
          return (
            <div className="container"  style={{marginTop: '2%',maxWidth:'75%'}}>
			  <div className="col-lg-12">
					<div className="col-lg-12" style={{padding:'0'}}>
						<div className="col-lg-8" style={{float:'left', padding:'0'}}>
							<h5 style={{color: '#00909e'}}>LISTES DES ARTICLES ({data.articles.length})</h5>
						</div>
						<div className="col-lg-4" style={{float:'right', padding:'0',paddingBottom:'2%'}}>
							<form action={`/rechercher`}>
								<input type="search" className="form-control" name="titre" placeholder="Rechercher un titre d'article" />
							</form>
						</div>
					</div>
					<div className="main-card mb-3 card col-lg-12">
						<div className="card-body">
							<div className="table-responsive">
								<table className="mb-0 table table-striped">
									<thead>
									<tr>
										<th>#</th>
										<th>Titre</th>
										<th>Auteur</th>
										<th>Date de publication</th>
									</tr>
									</thead>
									<tbody>
									{data.articles.map((article, index) => (
										<tr key={index}>
										  <td>{index+1}</td>
										  <td><Link to={`/show/${article._id}`}>{article.title}</Link></td>
										  <td>{article.title}</td>
										  <td>{article.publisher}</td>
										</tr>
									  ))}
									</tbody>
								</table>
							</div>
							<button className="mb-2 mr-2 btn btn-secondary" style={{marginTop: '2%'}}><Link to="/create" style={{color: '#FFFFFF'}}>Ajouter une article</Link></button>
						</div>
					</div>
				</div>
            </div>
          );
        }}
      </Query>
    );
  }
}

export default App;
