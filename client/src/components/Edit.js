import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import gql from "graphql-tag";
import { Query, Mutation } from "react-apollo";

const GET_ARTICLE = gql`
    query article($articleId: String) {
        article(id: $articleId) {
            _id
            title
            author
            content
            publisher
        }
    }
`;

const UPDATE_ARTICLE = gql`
    mutation updateBook(
        $id: String!,
        $title: String!,
        $author: String!,
        $content: String!) {
        updateBook(
        id: $id,
        title: $title,
        author: $author,
        content: $content,) {
            publisher
        }
    }
`;

class Edit extends Component {

  render() {
    let title, author, content;
    return (
        <Query query={GET_ARTICLE} variables={{ articleId: this.props.match.params.id }}>
            {({ loading, error, data }) => {
                if (loading) return 'Loading...';
                if (error) return `Error! ${error.message}`;
        
                return (
                    <Mutation mutation={UPDATE_ARTICLE} key={data.article._id} onCompleted={() => this.props.history.push(`/`)}>
                        {(updateBook, { loading, error }) => (
                            <div className="container" style={{marginTop: '2%',maxWidth:'65%'}}>
                                <div className="panel panel-default">
                                    <div className="panel-heading">
                                        <h4 className="panel-title" style={{color: '#00909e',marginBottom:'2%'}}>
                                            Modifier article
                                        </h4>
                                    </div>
                                    <div className="panel-body">
                                        <h4><Link to="/" className="btn btn-primary">Liste des articles</Link></h4>
                                        <form onSubmit={e => {
                                            e.preventDefault();
                                            updateBook({ variables: { id: data.article._id, title: title.value, author: author.value, content: content.value } });
                                            title.value = "";
                                            author.value = "";
                                            content.value = "";
                                        }}>
                                            <div className="form-group">
                                                <label htmlFor="title">Titre:</label>
                                                <input type="text" className="form-control" name="title" ref={node => {
                                                    title = node;
                                                }} placeholder="Title" defaultValue={data.article.title} />
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="author">Auteur:</label>
                                                <input type="text" className="form-control" name="author" ref={node => {
                                                    author = node;
                                                }} placeholder="Author" defaultValue={data.article.author} />
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="content">Contenu:</label>
                                                <textarea className="form-control" name="content" ref={node => {
                                                    content = node;
                                                }} placeholder="Content" cols="80" rows="3" defaultValue={data.article.content} />
                                            </div>
                                            <button type="submit" className="btn btn-success">Modfier</button>
											&nbsp;&nbsp;
											<Link to="/" className="btn btn-secondary">Revenir à la liste des articles</Link>
                                        </form>
                                        {loading && <p>Loading...</p>}
                                        {error && <p>Error :( Please try again</p>}
                                    </div>
                                </div>
                            </div>
                        )}
                    </Mutation>
                );
            }}
        </Query>
    );
  }
}

export default Edit;