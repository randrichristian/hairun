import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../App.css';
import gql from 'graphql-tag';
import { Query, Mutation } from 'react-apollo';

const GET_ARTICLE = gql`
    query article($title: String) {
        article(id: $title) {
            _id
            title
            author
            content
            publisher
        }
    }
`;
class Find extends Component {

  render() {
    return (
        <Query pollInterval={500} query={GET_ARTICLE} variables={{ title: this.props.match.params.id }}>
            {({ loading, error, data }) => {
                if (loading) return 'Loading...';
                if (error) return `Error! ${error.message}`;
        
                return (
                    <div className="container" style={{marginTop: '2%',maxWidth:'50%'}}>
						{data.articles.length}
                    </div>
                );
            }}
        </Query>
    );
  }
}

export default Find;