import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../App.css';
import gql from 'graphql-tag';
import { Query, Mutation } from 'react-apollo';

const GET_ARTICLE = gql`
    query article($articleId: String) {
        article(id: $articleId) {
            _id
            title
            author
            content
            publisher
        }
    }
`;

const DELETE_ARTICLE = gql`
  mutation removeArticle($id: String!) {
    removeArticle(id:$id) {
      _id
    }
  }
`;

class Show extends Component {

  render() {
    return (
        <Query pollInterval={500} query={GET_ARTICLE} variables={{ articleId: this.props.match.params.id }}>
            {({ loading, error, data }) => {
                if (loading) return 'Loading...';
                if (error) return `Error! ${error.message}`;
        
                return (
                    <div className="container" style={{marginTop: '2%',maxWidth:'50%'}}>
                        <div className="panel panel-default">
                            <div className="panel-heading">
                                <h5 className="panel-title" style={{color: '#00909e',marginBottom:'2%'}}>
                                {data.article.title}
                                </h5>
                            </div>
                            <div className="panel-body">
                                <dl>
                                    <dt>Auteur de l'article:</dt>
                                    <dd>{data.article.author}</dd>
                                    <dt>Contenu:</dt>
                                    <dd>{data.article.content}</dd>
                                    <dt>Date de publication:</dt>
                                    <dd>{data.article.publisher}</dd>
                                </dl>
                                <Mutation mutation={DELETE_ARTICLE} key={data.article._id} onCompleted={() => this.props.history.push('/')}>
                                    {(removeArticle, { loading, error }) => (
                                        <div>
                                            <form
                                                onSubmit={e => {
                                                    e.preventDefault();
                                                    removeArticle({ variables: { id: data.article._id } });
                                                }}>
                                                <Link to={`/edit/${data.article._id}`} className="btn btn-success">Modifier</Link>&nbsp;&nbsp;
                                                <button type="submit" className="btn btn-danger">Supprimer</button>&nbsp;&nbsp;
												<Link to="/" className="btn btn-secondary">Revenir à la liste des articles</Link>
                                            </form>
                                        {loading && <p>Loading...</p>}
                                        {error && <p>Error :( Please try again</p>}
                                        </div>
                                    )}
                                </Mutation>
                            </div>
                        </div>
                    </div>
                );
            }}
        </Query>
    );
  }
}

export default Show;