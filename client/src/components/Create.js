import React, { Component } from 'react';
import gql from "graphql-tag";
import { Mutation } from "react-apollo";
import { Link } from 'react-router-dom';

const ADD_ARTICLE = gql`
    mutation AddArticle(
        $title: String!,
        $author: String!,
        $content: String!,
        ) {
        addArticle(
            title: $title,
            author: $author,
            content: $content) {
            _id
        }
    }
`;

class Create extends Component {
  
    render() {
      let isbn, title, author, content;
      return (
        <Mutation mutation={ADD_ARTICLE} onCompleted={() => this.props.history.push('/')}>
            {(addArticle, { loading, error }) => (
                <div className="container" style={{marginTop: '2%',maxWidth:'50%'}}>
                    <div className="panel panel-default">
                        <div className="panel-heading">
                            <h3 className="panel-title" style={{color: '#00909e',marginBottom:'2%'}}>
                                AJOUTER UNE ARTICLE
                            </h3>
                        </div>
                        <div className="panel-body">
                            <form onSubmit={e => {
                                e.preventDefault();
                                addArticle({ variables: { title: title.value, author: author.value, content: content.value } });
                                isbn.value = "";
                                title.value = "";
                                author.value = "";
                                content.value = "";
                            }}>
                                <div className="form-group">
                                    <label htmlFor="title">Titre:</label>
                                    <input type="text" className="form-control" name="title" ref={node => {
                                        title = node;
                                    }} placeholder="Titre de l'article..." />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="author">Auteur:</label>
                                    <input type="text" className="form-control" name="author" ref={node => {
                                        author = node;
                                    }} placeholder="Auteur..." />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="content">Contenu:</label>
                                    <textarea className="form-control" name="content" ref={node => {
                                        content = node;
                                    }} placeholder="Contenu..." cols="80" rows="3" />
                                </div>
								<h5><Link to="/" className="btn btn-secondary">Revenir à la liste des articles</Link></h5>
                                <button type="submit" className="btn btn-success">Ajouter l'article</button>
                            </form>
                            {loading && <p>Loading...</p>}
                            {error && <p>Error :( Please try again</p>}
                        </div>
                    </div>
                </div>
            )}
        </Mutation>
      );
    }
  }
  
  export default Create;